> 🚧 **This page is a work in progress and is currently incomplete. Thanks for understanding!** ❤️

---

**Table of Contents**

[[_TOC_]]

---

# Privacy Baby Steps

This page is designed to be the simplest possible starting point to begin your digital privacy journey.

It is split into small, ordered, and achievable steps: your privacy baby steps.

The **🟢 EASY** steps in this page are the **absolute minimum** one should do to take care of their digital privacy. Think of these steps as low-hanging fruits *(low effort, high reward)*.

As you progress toward the **🟡 MEDIUM** and **🔴 HARD** steps, more effort will be required and each step will progressively get more involved, resulting in less privacy gain *("the reward")* for the amount of time you need to put in *("the effort")*.

## The Privacy Tradeoff

In general, privacy is a tradeoff: you usually sacrifice some convenience to gain some privacy.

For example, you probably put a passcode on your smartphone to prevent people from looking at your stuff. Now, you have to type a passcode every time you need to unlock your phone.

It is however possible to make this tradeoff worthwhile: sacrifice a little bit of convenience to gain a lot of privacy.

For example, if you enable facial/thumbprint recognition on your phone *(also called biometrics)*, now your phone unlocks with just a tiny bit more effort, but you're still preventing most people from easily getting into your phone, which is a massive privacy gain *(small loss of convenience for large gain of privacy)*.

## Steps Order and When to Stop

The steps in this guide are meant to be followed and applied in order.

The **🟢 EASY** steps will not only require less time and effort, but will also increase privacy greatly for just a small loss of convenience *(and sometimes arguably no convenience loss at all)*.

As you move to the **🟡 MEDIUM** steps and maybe even the **🔴 HARD** steps, the privacy tradeoff will get less and less interesting for most people. It is up to you to decide where you stop.

If you are unsure and would like to learn more, you can start reading and thinking about what is your [threat model](https://www.privacyguides.org/en/basics/threat-modeling/). Here's another [great simple guide](https://thenewoil.org/en/guides/prologue/threat-model/).

## Target Audience

This guide is meant for the general public. In other words, anyone. No particular computer skill or knowledge is required.

Also, this guide is specifically designed for people who don't want to invest a lot of time into fixing their digital privacy *(which, from experience, is most people)*.

The goal is to offer a lot of value out of minimal effort.

This guide may be less interesting to privacy enthusiasts who probably already know most of this stuff.

## Tone and Writing Style

This page is written with a relatively authoritative tone. It has many statements in the form of:

> You should do X.

> Stay away of Y.

> Never do Z.

Keep in mind this writing style is chosen purely for brievely purposes to keep explanations concise and easy to read.

All advice provided by this guide is merely the opinion of the author.

## Other Great Guides

There are already many great privacy guides online. Here are a few of the top ones:

- [Privacy Guides](https://privacyguides.org/)
- [The New Oil](https://thenewoil.org/)
- [Techlore](https://techlore.tech/)
- [Surveillance Self-Defense](https://ssd.eff.org/)

I recommend reading them if you would like to learn more. They go well beyond the scope of this simple page. 

## Why Should I Care About Privacy?

It is very common for people to minimize the value or relevance of their own privacy rights.

People will often say things like:

> I have nothing to hide.

> I'm not doing anything illegal.

> I'm an open book.

> Why would privacy even matter to me?

In [this TED Talk](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters), journalist [Glenn Greenwald](https://en.wikipedia.org/wiki/Glenn_Greenwald) explains really well, in plain English, why privacy should be a big deal to everyone.

#  Step 1: **Remove Unnecessary Apps**

**Difficulty Level: 🟢 EASY**

## The Short Story

✔️ Remove all apps you don't frequently use from your smartphone.

✔️ Do the same for all your desktop/laptop computers.

✔️ Also remove all internet browser extensions that you don't absolutely need.

## The Long Story

### Reducing Attack Surface

All software is written with code and all code has bugs. Bugs can can exploited to create security vulnerabilities in software, which attackers can use against you. Some security vulnerabilities are so bad they can allow remote attackers to take control of your computer.

Each extra application on your smartphone or laptop/desktop computer means extra code that could contain security vulnerabilities.

In other words, removing software directly results in reducing the potential security vulnerabilities that could be present on your devices.

In information security language, this is called reducing your attack surface.

---

### Security vs. Privacy

Now, security and privacy are two different concepts, but they are directly related.

If you devices gets compromised ("hacked"), the attacker could gain access to your private information, which results in a violation of your privacy.

Therefore, security is often required to protect privacy.

# Step 2: **Install Software Updates**

**Difficulty Level: 🟢 EASY**

*coming soon!*

# Step 3: **Review Mobile App Permissions**

**Difficulty Level: 🟢 EASY**

*coming soon*

# Step 4: **Switch Internet Browser**

**Difficulty Level: 🟢 EASY**

## The Short Story

✔️ Stop using Google Chrome and Microsoft Edge, switch to [Firefox](https://www.mozilla.org/en-CA/firefox/new/).

## The Long Story

Statistically, most people use Google Chrome to browse the web, as it currently [dominates the browser market](https://gs.statcounter.com/browser-market-share).

However, Google being the world's largest advertising company, its browser is one of the main data collection tools in its arsenal. It is **terrible** for your privacy.

Don't just take my word for it, Chrome has been [declared spy software by The Washington Post](https://www.washingtonpost.com/technology/2019/06/21/google-chrome-has-become-surveillance-software-its-time-switch/).

Microsoft, another big tech corporation worth trillions of dollars, doesn't have a great track record either when it comes to respecting your digital privacy.

Unsurprisingly, its browser, Microsoft Edge, does not fare much better than Chrome:

> According to Douglas J. Leith, a computer science professor from Trinity College, Dublin, Microsoft Edge is among the least private browsers. [(Wikipedia)](https://en.wikipedia.org/wiki/Microsoft_Edge)

> A professor says Edge is the worst for privacy. Microsoft isn't happy [(ZDNET)](https://www.zdnet.com/article/a-professor-says-edge-is-the-worst-for-privacy-microsoft-isnt-happy/)

> Microsoft Edge is leaking the sites you visit to Bing [(The Verge)](https://www.theverge.com/2023/4/25/23697532/microsoft-edge-browser-url-leak-bing-privacy)

If you're using Google Chrome or Microsoft Edge, your best move is to switch to [Firefox](https://www.mozilla.org/en-CA/firefox/new/).

Firefox is made by the [Mozilla Corporation](https://en.wikipedia.org/wiki/Mozilla_Corporation), which ultimately reports to the [Mozilla Foundation](https://en.wikipedia.org/wiki/Mozilla_Foundation), a non-profit organisation.

As opposed to giant companies like Google and Microsoft, Mozilla is not structured to make profits for investors, but rather to make the internet a better place for everyone:

> The Mozilla Foundation describes itself as "a non-profit organization that promotes openness, innovation and participation on the Internet". The Mozilla Foundation is guided by the Mozilla Manifesto, which lists 10 principles which Mozilla believes "are critical for the Internet to continue to benefit the public good as well as commercial aspects of life". [(Wikipedia)](https://en.wikipedia.org/wiki/Mozilla_Foundation)

As opposed to many other browsers, Firefox has evolved over the years to better protect your privacy.

For further details on the inner workings of Mozilla, here's a [podcast with Mitchell Baker, the CEO of Mozilla](https://www.theverge.com/2023/2/14/23598344/mozilla-firefox-ceo-mitchell-baker-microsoft-edge-bing-google-apple-ai).

# Step 5: **Switch Search Engine**

**Difficulty Level: 🟢 EASY**

## The Short Story

✔️ Stop using Google and Bing for search, switch to [DuckDuckGo](https://duckduckgo.com/).

## The Long Story

*coming soon!*

# Step 6: **Use a Password Manager**

**Difficulty Level: 🟢 EASY**

## Summary

✔️ If you're not already using a password manager, start using one. The best **free** password manager for most people is [Bitwarden](https://bitwarden.com/). [1Password](https://1password.com/) is another excellent option. It is not free like Bitwarden, but it may be a bit more intuitive and easy to use for people who are less tech-savvy. Here's a [detailed review and recommendation](https://www.nytimes.com/wirecutter/reviews/best-password-managers/) of both by Wirecutter.

## The Long Story

Everyone should be using a password manager. Plain and simple.

The security gains *(and consequential privacy gains)* totally outweighs the extra inconvenience.

Once you get the hang of it, one could argue it is even *more* convenient to use a password mananger versus not using one:

- The mental burden of having to remember dozens of passwords is completely gone. You only need to remember a single password: the password to your password manager. Think of it as the combination to your secure vault.

- Passwords automatically fill themselves into websites and apps. This is a common feature of password managers. You no longer have to manually type any passwords.

- Since passwords are being auto-filled by your password manager, you can choose unique, very long, and complex passwords on all your accounts, without sacrificing convenience in the process.

- As you progressively collect all your passwords into your password manager, a list of all your online accounts is being created. This is often called your *digital footprint*. It is the presence you have on the internet; a list of all your digital assets. Having this list helps for many reasons, including when you want to review the security of accounts you already have; when you want to delete old accounts you no longer want; and for [digital inheritance](https://en.wikipedia.org/wiki/Digital_inheritance).

If you're still unconvinced, Wirecutter has a [great explainer](https://www.nytimes.com/wirecutter/blog/why-you-need-a-password-manager-yes-you/) on the necessity of password managers.

# Step X: **Switch Mobile Operating System**

**Difficulty Level: 🟡 MEDIUM**

## The Short Story

✔️ Stop using Android, switch to iOS.

## The Long Story

*coming soon!*

# Step X: **Switch Email Provider**

**Difficulty Level: 🔴 HARD**

*coming soon!*

# Step X: **Delete Unused Accounts**

**Difficulty Level: 🔴 HARD**

## The Short Story

*coming soon!*

## The Long Story

*coming soon!*

# Step X: **Switch Desktop Operating System**

**Difficulty Level: 🔴 HARD**

## The Short Story

✔️ If you are using Windows as your desktop operating system (OS), switch to either macOS or Linux.

Linux is the absolute best OS for privacy. macOS is not as good, but it is leaps and bounds better than Windows.

## The Long Story

*coming soon!*
